---
layout: page
title: "Device Seized"
author: RaReNet
language: en
summary: "Is your device lost? Has it been stolen or seized by a third party? In any of these incidences it is very important to get a clear picture of what happened, what kinds of data and accounts may be vulnerable as a result and what steps must be taken to prevent the leaking and misuse of your information, contacts and accounts."
date: 2018-09
permalink: /en/topics/device-seized/
parent: /en/
---

# My Device has been seized, if only for a few hours.

Is your device lost? Has it been stolen or seized by a third party? In any of these incidences it is very important to get a clear picture of what happened, what kinds of data and accounts may be vulnerable as a result and what steps must be taken to prevent the leaking and misuse of your information, contacts and accounts.

## Workflow

### device_still_missing

Do you have the device with you?

  - [Yes](#have_device)
  - [No](#lost_device)

### have_device

> Are there signs that the device has been physically tampered with?
> To determine if this is possible it is a good idea to go over these questions:
> - How long was the device out of your sight?
> - Who potentially could have had access to it?
> - Why would they want access to it?
> - Are there signs that the device has been physically tampered with?
>
> For more extensive assessment see [Threat modelling, Surveillance Self Defense Guide](https://ssd.eff.org/risk/threats).

Are you using this device now to access this site?

  - [Yes](#using_seized_device)
  - [No](#using_device)

### using_seized_device

We recommend that you use another device and come back to this site.

  - [Continue, I need to use this device](#using_device)

### lost_device

> If your device is lost or seized by a third party and you did not get it back, the first steps to take are the following:
>
> - Step 1: When your device has access to accounts (email, social media or web account) remove the authorization for this device for all accounts. This can be done by going to your accounts online and changing the account permissions. If you lost access to any accounts please refer to the section [I cannot access my account](AccountHijacking).
> - Step 2: Change the passwords for all accounts that are accessible by this device. To create a strong password go to [Security in a Box: Create and Maintain Strong Passwords](https://securityinabox.org/en/guide/passwords/)
> - Step 3: Turn on 2-factor authentication for all accounts that were accessible by this device. Please note that not all accounts support 2-factor authentication [See 2-factor notes from 'Account Hijack' section].
> - Step 4: If you have a tool installed on your lost devices that allows you to erase the data and the history of your device, use it.

Don't stop here!!

  - [Continue with next steps](#next_steps)

### using_device

> A hard drive can be taken from your computer and copied in a a couple of hours. If your information was encrypted with a password then it could take days for them to read your information. If it had a strong encryption password and they have no way of having or guessing it then it would be very possible your data is safe.

Do you think they had access to your information?

  - [Yes](#data_compromised)
  - [I am not certain, but they might or I do not know](#data_compromised)
  - [No, I believe my data is safe](#next_steps)

### data_compromised

> It is important to take some steps to protect further risks by fallowing this steps
>
> - Step 1: When your device has access to accounts (email, social media or web account) remove the authorization for this device for all accounts. This can be done by going to your accounts online and changing the account permissions. If lost access to any accounts please refer to the section [I cannot access my account](AccountHijacking).
> - Step 2: Change the passwords for all accounts that are accessible by this device. To create a strong password go to [Security in a Box: Create and Maintain Strong Passwords](https://securityinabox.org/en/guide/passwords/)
> - Step 3: Turn on 2-factor authentication for all accounts that were accessible by this device. Please note that not all accounts support 2-factor authentication [See 2-factor notes from 'Account Hijack' section].

Are you planning on using your seized device again?

  - [No](#next_steps)
  - [Yes](#clean_device)

### clean_device

> If you have lost contact with your device for an extended period of time and you feel there is a chance that something has been installed on it, please consider the following:

> - **Computer**: reinstall the OS from scratch and recover all documents from the last backup and scan all your documents and files with antivirus software. For more guidance on this, see cleaning up your device in the malware section. In some cases, reinstalling the OS might not be enough. Reinstalling BIOSes and firmwares from a trusted source might be required.
> - **Phones and tablets**: Depending on your level of risk and the circumstances under which your mobile phone or tablet was taken, it may be advisable to not use it again. If possible, migrate all of the data off of your phone or tables and purchase a new one. If you cannot change devices but you suspect it might be compromised, take precautions and do not use your phone or tablet for sensitive communication or opening sensitive files. Do not take it with you when going to sensitive meetings or have it with you when discussing sensitive topics.

Don't stop here!!

  - [Continue with next steps](#next_steps)

### next_steps

Important next steps, whether your device is still lost or you have it back, complete the following steps:

- Step 1: Think about what you used this device for - is there sensitive information on this device, such as your contacts, location or the content of your messages? Can this data be problematic for someone?
- Step 2: Inform your network. Inform the key and high-risk contacts you work with privately. If you feel comfortable doing so, post a list of potentially compromised accounts on your website or a social media account.
- Step 3: Do you use the same password on other accounts or devices? If so, perform this process on those accounts. They may also be compromised.
- Step 4: If possible, review the connection history/account activity of all accounts connected to the device (available feature on Facebook, [Gmail](https://www.google.com/settings/security?hl=en) and other email providers). Check to see if your account was used at a time when you were not online or if your account was accessed from an  unfamiliar location or IP address. See the Account Hijacking section for further details.
- Step 5: Check the account settings of all accounts connected to the device. Have they been changed? For email accounts, check for auto-forwards, possible changes to the backup/reset email address of phone numbers, synchronization to different devices, including phones, computers or tablets, and permissions to applications or other account permissions.
- Step 6: Repeat the review of the connection history/account activity - at least once a week for a month - to ensure that your account does not continue to show strange activity. If the history/account activities continue to show strange activity, proceed to the malware section.

## Take extra precautions against attackers:

Prevention is the key to mitigating the risk of having your device seized, lost or stolen. However, simple actions can protect the data on your device if it is seized. Think about encryption, passwords, pin code locks for cell phone backups, tools that allow remote data wipes, installation of alert software in the case of theft. [Prey Anti-Theft](https://preyproject.com/) is a useful cross-platform and open source device tracking tool.

## Investigate

If your device has been stolen or seized by a third party, it is good to understand why this has happened. Who do you think might be interested in targeting you or your organization? Is this threat related to your work? In the section on helpful resources there are links to guides that provide tips and tricks on how to prevent digital emergencies and be proactive about your digital security.

## Helpful resources

* [Security in a Box](https://securityinabox.org/en/chapter_7_2)
* [Threat modeling, Surveillance Self Defense Guide](https://ssd.eff.org/risk/threats)
